//----------------------------------------------------------------------------
#ifndef OptionsH
#define OptionsH
//----------------------------------------------------------------------------
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>
#include "cspin.h"
#include <Dialogs.hpp>
#include <mem.h> // for memcpy
//----------------------------------------------------------------------------
class TOptionsBox : public TForm
{
__published:
	TPanel *Panel1;
    TPageControl *RichPadOptions;
    TTabSheet *HistoryOptions;
    TRadioGroup *StyleGroup;
    TRadioGroup *LocationGroup;
    TCSpinEdit *HistoryItems;
    TLabel *Label1;
    TButton *ClearHistory;
    TLabel *Label2;
        TTabSheet *FileTypeOptions;
    TButton *OKBtn;
    TButton *CancelBtn;
    TTabSheet *MiscPage;
    TButton *DeleteKey;
    TGroupBox *DefaultFontGroup;
    TComboBox *DefFontCBox;
    TButton *SetFontBtn;
    TButton *DefFontResetBtn;
    TFontDialog *DefFontDialog;
    TCheckBox *DefWordWrap;
        TCheckBox *SaveColor;
        TCheckBox *SaveStyle;
        TCheckBox *DefToolBar;
        TCheckBox *DefFormatBar;
        TLabel *Label3;
        TCheckBox *DefStatBar;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall ClearHistoryClick(TObject *Sender);
    void __fastcall CancelBtnClick(TObject *Sender);
    void __fastcall DeleteKeyClick(TObject *Sender);
    void __fastcall SetFontBtnClick(TObject *Sender);
    void __fastcall DefFontResetBtnClick(TObject *Sender);
    void __fastcall DefFontCBoxClick(TObject *Sender);
    
private:
    void ReadCheckBoxes(int ListIndex);
public:
	virtual __fastcall TOptionsBox(TComponent* AOwner);
};

struct FileTypeInfo
{
bool WordWrap;
bool ShowToolbar;
bool ShowFBar; // format bar
bool ShowStatBar;
};

// RichPadOptions structure
struct RichPadOptions
{
bool FullPath; // false = show file name only
bool InFileMenu; // false = history in seperate Reopen menu
int HistItems; // number of items to have in history list
// font options for TRichFonts
bool SaveStyle;
bool SaveColor;
// word wrap flags for different file types
FileTypeInfo NewFile; // new files
FileTypeInfo TXTFile; // .txt files
FileTypeInfo OtherFile; // all other files
};

//----------------------------------------------------------------------------
extern PACKAGE TOptionsBox *OptionsBox;
//----------------------------------------------------------------------------
#endif
