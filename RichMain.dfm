ÿ
 TSCRATCHPAD 0<  TPF0TScratchPad
ScratchPadLeftÀ TopsWidth HeightwCaptionRichPad - UntitledColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style MenuMainMenuOldCreateOrder	ShowHint	OnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 
TStatusBar	StatusBarLeft Top6WidthHeightPanelsWidthE TextLine 1; Pos. 1Widthd Text
UnmodifiedWidth2  	PopupMenu	PopupViewSimplePanel  TCoolBarCoolBarLeft Top WidthHeight2AutoSize	BandsControlMainToolBar
ImageIndexÿ	MinHeightWidth Control	FormatBar
ImageIndexÿ	MinHeightWidth  	PopupMenu	PopupView TToolBarMainToolBarLeft	Top WidthHeightEdgeBorders Flat	Images
ImageList1TabOrder  TToolButton
FileNewBtnLeft Top HintNew|Create a new fileCaption
FileNewBtnEnabled
ImageIndex OnClickFileNewClick  TToolButtonFileOpenBtnLeftTop HintOpen|Open an existing fileCaptionFileOpenBtn
ImageIndexOnClickFileOpenClick  TToolButtonFileSaveBtnLeft.Top HintSave|Save the current fileCaptionFileSaveBtnEnabled
ImageIndexOnClickFileSaveClick  TToolButtonToolButton2LeftETop WidthCaptionToolButton2
ImageIndex	StyletbsSeparator  TToolButtonFilePrintBtnLeftMTop Hint4Print immediately|Print the current file immediatelyCaptionFilePrintBtn
ImageIndexOnClickFilePrintBtnClick  TToolButton	SearchBtnLeftdTop Hint.Search|Search for text within the current fileCaption	SearchBtnEnabled
ImageIndexOnClickEditFindClick  TToolButtonToolButton1Left{Top WidthCaptionToolButton1
ImageIndexStyletbsSeparator  TToolButton
EditCutBtnLeft Top Hint*Cut|Cut the selected text to the clipboardCaption
EditCutBtnEnabled
ImageIndexOnClickEditCutClick  TToolButtonEditCopyBtnLeft Top Hint,Copy|Copy the selected text to the clipboardCaptionEditCopyBtnEnabled
ImageIndexOnClickEditCopyClick  TToolButtonEditPasteBtnLeft± Top Hint7Paste|Paste the contents of the clipboard to the cursorCaptionEditPasteBtnEnabled
ImageIndexOnClickEditPasteClick  TToolButtonUndoBtnLeftÈ Top HintUndo|Undo the last actionCaptionUndoBtnEnabled
ImageIndexOnClickEditUndoClick   TToolBar	FormatBarLeft	TopWidthHeightEdgeBorders Flat	Images
ImageList2TabOrder 	TComboBoxFontBoxLeft Top Width HeightHintFont|Change the fontStylecsOwnerDrawFixed
ItemHeightSorted	TabOrder OnClickFontBoxClick
OnDrawItemFontBoxDrawItem
OnDropDownFontBoxDropDown  	TComboBoxSizeBoxLeft Top Width0HeightHint-Font size|Change the size of the current font
ItemHeightItems.Strings1234567891011121314151617181920 TabOrderOnClickSizeBoxClick
OnKeyPressSizeBoxKeyPress  TToolButtonToolButton11LeftÁ Top WidthCaptionToolButton11
ImageIndexStyletbsSeparator  TToolButtonFontBoldBtnLeftÉ Top HintBold|Toggle bold textCaptionFontBoldBtn
ImageIndex StyletbsCheckOnClickFontBoldBtnClick  TToolButtonFontItalicBtnLeftà Top HintItalics|Toggle italicsCaptionFontItalicBtn
ImageIndexStyletbsCheckOnClickFontItalicBtnClick  TToolButtonFontUnderBtnLeft÷ Top HintUnderline|Toggle underliningCaptionFontUnderBtn
ImageIndexStyletbsCheckOnClickFontUnderBtnClick  TToolButtonToolButton12LeftTop WidthCaptionToolButton12
ImageIndexStyletbsSeparator  TToolButtonAlignLeftBtnLeftTop Hint(Align Left|Align text on the left marginCaptionAlignLeftBtnDown	Grouped	
ImageIndexStyletbsCheckOnClickAlignLeftBtnClick  TToolButtonAlignCenterBtnLeft-Top HintAlign Center|Center textCaptionAlignCenterBtnGrouped	
ImageIndexStyletbsCheckOnClickAlignCenterBtnClick  TToolButtonAlignRightBtnLeftDTop Hint*Align Right|Align text on the right marginCaptionAlignRightBtnGrouped	
ImageIndexStyletbsCheckOnClickAlignRightBtnClick  TToolButtonToolButton13Left[Top WidthCaptionToolButton13
ImageIndexStyletbsSeparator  TToolButton	BulletBtnLeftcTop HintBullets|Toggle bulleting
AllowAllUp	Caption	BulletBtn
ImageIndexStyletbsCheckOnClickBulletBtnClick    	TRichEditMemoLeft Top2WidthHeightAlignalClientHideSelection	PopupMenu	MemoPopup
ScrollBarsssBothTabOrderWantTabs	OnChange
MemoChangeOnSelectionChangeMemoSelectionChange  TPanel	PopupEditLeft Top5Width9Height	AlignmenttaLeftJustify
BevelInnerbvRaised
BevelOuter	bvLoweredColorclWhiteCtl3D	ParentCtl3DTabOrderVisible  	TMainMenuMainMenuImages
ImageList1LeftTop 	TMenuItemFileMenuCaption&File 	TMenuItemFileNewCaption&NewEnabled
ImageIndex ShortCutN@OnClickFileNewClick  	TMenuItemFileOpenCaption&Open...
ImageIndexShortCutO@OnClickFileOpenClick  	TMenuItemFileSaveCaption&SaveEnabled
ImageIndexShortCutS@OnClickFileSaveClick  	TMenuItem
FileSaveAsCaptionSave &As...EnabledOnClickFileSaveAsClick  	TMenuItemN1Caption-  	TMenuItem	FilePrintCaption	&Print...
ImageIndexShortCutP@OnClickFilePrintClick  	TMenuItemFilePrintSetupCaptionPrint Set&up...OnClickFilePrintSetupClick  	TMenuItemMRUTopCaption-  	TMenuItem	MRUBottomCaption-Visible  	TMenuItemFileExitCaptionE&xitOnClickFileExitClick   	TMenuItemEdit1Caption&Edit 	TMenuItemEditUndoCaption&UndoEnabled
ImageIndexShortCutZ@OnClickEditUndoClick  	TMenuItemN5Caption-  	TMenuItemEditCutCaptionCu&tEnabled
ImageIndexShortCutX@OnClickEditCutClick  	TMenuItemEditCopyCaption&CopyEnabled
ImageIndexShortCutC@OnClickEditCopyClick  	TMenuItem	EditPasteCaption&PasteEnabled
ImageIndexShortCutV@OnClickEditPasteClick  	TMenuItemEditSelectAllCaptionSelect &AllEnabledShortCutA@OnClickEditSelectAllClick  	TMenuItemN4Caption-  	TMenuItemEditFindCaption&Find...Enabled
ImageIndexShortCutF@OnClickEditFindClick  	TMenuItemEditFindNextCaption
Find &NextEnabledShortCutrOnClickFindDialogFind  	TMenuItemEditReplaceCaptionR&eplace...EnabledShortCutH@OnClickEditReplaceClick   	TMenuItemFormat1CaptionF&ormat 	TMenuItemFormatSelectFontCaption&Select Font...OnClickSelectFontClick  	TMenuItemFormatDefaultFontCaptionSet &Default Font...OnClickFormatDefaultFontClick  	TMenuItemFormatReformatCaptionReformat...OnClickFormatReformatClick  	TMenuItemFormatBulletsCaption&BulletsOnClickFormatBulletsClick  	TMenuItemFormatWordWrapCaption
&Word WrapChecked	OnClickEditWordWrapClick   	TMenuItemView1Caption&View 	TMenuItemViewToolbarCaptionToolbarChecked	OnClickViewToolbarClick  	TMenuItemViewFormatBarCaption
Format BarChecked	OnClickViewFormatBarClick  	TMenuItemViewStatusBarCaption
Status BarChecked	OnClickViewStatusBarClick  	TMenuItemViewOptionsCaption
Options...OnClickViewOptionsClick   	TMenuItemHelp1Caption&Help 	TMenuItemHelpContentsCaption	&ContentsEnabled  	TMenuItem	HelpAboutCaption	&About...OnClickHelpAboutClick   	TMenuItemFormatBarShortcuts1CaptionFormat Bar ShortcutsVisible 	TMenuItemBold1CaptionBoldShortCutB@OnClickFontBoldBtnClick  	TMenuItemItalic1CaptionItalicShortCutI@OnClickFontItalicBtnClick  	TMenuItem
Underline1Caption	UnderlineShortCutU@OnClickFontUnderBtnClick  	TMenuItemLeftJustify1CaptionLeft JustifyShortCutL@OnClickAlignLeftBtnClick  	TMenuItemCenter1CaptionCenterShortCutM@OnClickAlignCenterBtnClick  	TMenuItemRightJustify1CaptionRight JustifyShortCutR@OnClickAlignRightBtnClick    TOpenDialog
OpenDialogFilterDRich Text Files (*.rtf)|*.rtf|Text Files (*.txt)|*.txt|All Files|*.*Left(Top  TSaveDialog
SaveDialog
DefaultExt*.rtfFilterBRich Text File (*.rtf)|*.rtf|Text File (*.txt)|*.txt|All Files|*.*OptionsofOverwritePromptofHideReadOnly LeftHTop  
TPopupMenu	MemoPopupImages
ImageList1LeftHTop 	TMenuItem	PopupUndoCaption&Undo
ImageIndexOnClickEditUndoClick  	TMenuItemN3Caption-  	TMenuItemPopupCutCaptionCu&tEnabled
ImageIndexOnClickEditCutClick  	TMenuItem	PopupCopyCaption&CopyEnabled
ImageIndexOnClickEditCopyClick  	TMenuItem
PopupPasteCaption&PasteEnabled
ImageIndexOnClickEditPasteClick  	TMenuItemN6Caption-  	TMenuItemPopupSelectAllCaptionSelect &AllEnabledOnClickEditSelectAllClick   TFontDialog
FontDialogFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style MinFontSize MaxFontSize Left¨ Top  TPrinterSetupDialogPrinterSetupDialogLefthTop  TPrintDialogPrintDialogLeft Top  
TImageList
ImageList1Left(TopBitmap
&2  IL	 
    ÿÿÿÿÿÿÿÿÿÿÿÿÿBM6       6   (   @   0           0                                                                          tchPad->FocusControl(Memo);
 }
}
//--------------------------------------------------------------------------                                                        eÿÿÿwÎèûãÿÿÿw                                                         displayed

void __fastcall TScratchPad::FontBoxClick(TObject *Sender)
{
// change the font
if(FontBox->Text != "")
 {
 // change typeface name
 Memo->SelAttributes->Name = FontBox->Te                                                        -----------------------------------------------------------------------

// Format Bar Control event handlers

// note that if the font is changed via the Font or Size
//  ComboBoxs, the                                                         = !PopupSBar->Checked;
StatusBar->Visible = ViewStatusBar->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::ViewOptio                                                        -----------------------------------------------------------------

void __fastcall TScratchPad::ViewStatusBarClick(TObject *Sender)
{
// Hide / show status bar (StatusBar)
ViewStatusBar->                                                       ck(TObject *Sender)
{
// Hide / show format bar (CoolBar->Bands->Items[1])
ViewFormatBar->Checked = !ViewFormatBar->Checked; //toggle
PopupFBar->Checked = !PopupFBar->Checked;
CoolBar->Ba                                                  bar->Checked; // toggle
PopupTBar->Checked = !PopupTBar->Checked;
CoolBar->Bands->Items[0]->Visible = ViewToolbar->Checked;
}
//------------------------------------------------------------                                                   ------------------------------------------------------------------

// View menu event handlers

void __fastcall TScratchPad::ViewToolbarClick(TObject *Sender)
{
// Hide / show main tool                                                    -----------------------------------

// handler for print on main toolbar
void __fastcall TScratchPad::FilePrintBtnClick(TObject *Sender)
{
// print the document immediatly
Memo->Print(S                                                    ntDialog->Font->Assign(Memo->Font);
// set Memo->Font to font selected in dialog box
if(FontDialog->Execute())
 {
 Memo->Font->Assign(FontDialog->Font);
 MemoSelectionChange(Sender); // u                                                    e;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FormatDefaultFontClick(TObject *Sender)
{
// similar to Select Font, but                                                     ct *Sender)
{
FormatBullets->Checked = !FormatBullets->Checked;
// update the BulletBtn toolbutton
BulletBtn->Down = FormatBullets->Checked;
if(FormatBullets->Checked)
 Memo->Paragraph->                                                        the ComboBoxes
 }
// if no text is selected, all text typed after changing
//  the font appears in the new font!!!
}
//--------------------------------------------------------------------                                                        the selected text
FontDialog->Font->Assign(Memo->SelAttributes);
// set Memo selection font to font selected in dialog box
if(FontDialog->Execute())
 {
 Memo->SelAttributes->Assign(FontDi                                                        er)
{
AboutBox->ShowModal();
}
//---------------------------------------------------------------------------
//  Format Menu event handlers

void __fastcall TScratchPad::SelectFontClick                                                        ->FindText, 0, -1, options);
  }
 }
else if(Memo->SelLength > 0)
 Memo->SelText = ReplaceDialog->ReplaceText;
}
//------------------------------------------------------------------------                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                                                                                                                                            ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ             ¿ÿÿÿ                         ÿÿÿ                                                                                                                                                ÿÿÿ                     ÿÿÿ                 ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                                                                                                                                                  ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ             ¿ÿÿÿ            ¿ÿÿÿ                                                                                                                                     ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ   ÿÿÿ                     ÿÿÿ                 ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ   ¿ÿÿÿ                                                                                                                                   ÿÿÿ                   ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ             ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ                                                                                                                                            ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ   ÿÿÿ        ¿ÿÿÿ                                                                                                                                                                                ÿÿÿ                   ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ   ¿ÿÿÿ                                                                                                                                                                ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ   ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ                   ÿÿÿ                                                                                                                                                                           ÿÿÿ        ¿ÿÿÿ                                                                                                                                                                                                         ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ    ¿ÿÿÿ                                           ÿÿ          ÿÿ                                                                                                                                                ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ                                                              ÿÿ  ÿÿ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                                                                                                                                                                                             ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                 ÿÿ                                                                                                                       ÿÿ  ÿÿ  ÿÿ                                      ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ               ¿ÿÿÿ  ÿÿ¿ÿÿÿ                                                                                                                                                     ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                 ÿÿ ÿÿÿ  ÿÿ                                                                                                                                                   ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ               ¿ÿÿÿ  ÿÿ¿ÿÿÿ  ÿÿ                                                                                                                                                                              ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                 ÿÿ ÿÿÿ  ÿÿ ÿÿÿ  ÿÿ ÿÿÿ  ÿÿ ÿÿÿ  ÿÿ                                                                                                                                                          ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ               ¿ÿÿÿ  ÿÿ¿ÿÿÿ  ÿÿ¿ÿÿÿ  ÿÿ¿ÿÿÿ  ÿÿ¿ÿÿÿ                                                                                       ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                                    ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                 ÿÿ ÿÿÿ  ÿÿ                                                                                                                    ÿÿÿ                     ÿÿÿ                                   ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ                                                                                                                                                         ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ                              ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ    ¿ÿÿÿ                                                                                                                                                  ¿ÿÿÿ                    ¿ÿÿÿ                              ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ                                                                                                                                                        ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               BM>       >   (   @   0                               ÿÿÿ ÿÿreshFoÿÿ;
//Rÿÿistoryÿÿd
Draÿÿg chanÿ÷somethÁ÷in casÃûry lisÇûhe hisËû/ drawÜ÷ns));ÿPadOptÿÿeof(Riÿÿons, sÿÿ NewOpÿÿOptionÿÿÿÿÿÿÿÿÁùÿÿÿü GÑöÏþ  GÑö·þ   ö·þ    Aø·   "Aþ  "Aþ?  ÿ  ÑGþ? Áþ¿ ãüÃë¯ýß ãýßÿÿÿýßÿøÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÀÀà 1¿ëà 1 à 1~1à ~5à  à  êà ñà ñÀ
à ñààññààÿùñðàÿuõðàÿøÿÿÿÿÿÿÿÿ                        
TImageList
ImageList2LeftTopBitmap
&2  IL 
    ÿÿÿÿÿÿÿÿÿÿÿÿÿBM6       6   (   @   0           0                  ÇÇÎèKÿÿËKÉê_^[]Â UìSVW} ògF  _^[]Â Uìì@SMøV  WÛ¨F  _^[å]Ã¸    ÉtQÿPÃÌÌÌÌÌÌÌÌÌÌÌÌ    é1    è°             è°  è°  è°  RICHED32.dll ÌÌÌÌÌÌÌÌÌÌÌ ²ºî¡±MÿÌÞ¢       <          @             Symbol                        roblem: when we open a file in the TRichEdit,
//  it thinks the file has been modified, so we use
//  ClearModified()
if(OpenDialog->Execute())
 {
 if(OpenFile(OpenDialog-lÀtPÿÐ lÃVñÿ¶´  Æô  èâÿÿÿvàèÚÿÿÿ6èÓÿÿÇ    ^ÃUìSVWúñ]ÃKÀ~W>ReadString("OpenDir");
 OpenDialog->FileName = RichPadKey->ReadString("OpenFile");
 OpenDialog->FilterIndex = RichPadKey->ReadInteger("OpenIndex");
 }
catch(...)
 {
 OpenDialog->FileName = ""; // if it didn't work, set default
 }

// Here's the p. Save Changes?",
      "RichPad Message", MB_YESNOCANCEL | MB_ICONQUESTION);
 if(result == IDYES) FileSaveClick(0);
 if(result == IDCANCEL) return; // do nothing
 }
// Setup and execute File Open dialog
try
 {
 OpenDialog->InitialDir = RichPadKey------------------------

void __fastcall TScratchPad::FileOpenClick(TObject *Sender)
{
// Open a new file after seeing if current file needs to be saved
if(Memo->Modified)
 {
 int result = Application->MessageBox(
     "The current file has changedally, clear the caption and draw the history list
ScratchPad->Caption = "RichPad - Untitled";
RefreshFont(); // set Memo->Font to NewFont
// Refresh font will set modified
ClearModified(false);
}
//---------------------------------------------------- == IDCANCEL) return; // do nothing
 }
if(Memo->Lines->Count > 0)
 Memo->Clear(); //clears Memo->Modified
// add the last file opened to the history list
AddToHistory(SaveDialog->FileName);
DrawHistory(); // redraw
SaveDialog->FileName = "";
// fin one needs to be saved
if(Memo->Modified)
 {
 int result = Application->MessageBox(
     "The current file has changed. Save Changes?",
     "RichPad Message", MB_YESNOCANCEL | MB_ICONQUESTION);
 if(result == IDYES) FileSaveClick(Sender);
 if(resultform of command enabling. I prefer
//  to handle enabling only when neccessary instead of
//  through the OnIdle handler - speed over elegance.

void __fastcall TScratchPad::FileNewClick(TObject *Sender)
{
// create a new file after seeing if current 1);
}
//---------------------------------------------------------------------------

// Event handlers for Menus

//  File Menu event handlers
//---------------------------------------------------------------------------

// FileNewClick uses my ne = SendMessage(Memo->Handle,EM_LINEFROMCHAR,Memo->SelStart,0);
int pos = SendMessage(Memo->Handle,EM_LINEINDEX,line,0);
StatusBar->Panels->Items[POS_PANEL]->Text = String("Line ") +
 String(line + 1) + String("; Pos. ") + String(Memo->SelStart - pos +Memo->Paragraph->Numbering == nsBullet)
 {
 BulletBtn->Down = true;
 FormatBullets->Checked = true;
 }
else
 {
 BulletBtn->Down = false;
 FormatBullets->Checked = false;
 }
// finally, handle the line and position number in the status bar
int lightBtn->Down = true;

// handle font type and size combo boxes
FontBox->ItemIndex = FontBox->Items->IndexOf(Memo->SelAttributes->Name);
SizeBox->ItemIndex = SizeBox->Items->IndexOf(Memo->SelAttributes->Size);

// handle bullet menu item / button
if(
 // handle justification buttons
if(Memo->Paragraph->Alignment == taLeftJustify)
 AlignLeftBtn->Down = true;
else if(Memo->Paragraph->Alignment == taCenter)
 AlignCenterBtn->Down = true;
else if(Memo->Paragraph->Alignment == taRightJustify)
 AlignRibutes->Style.Contains(fsItalic))
 FontItalicBtn->Down = true;
else
 FontItalicBtn->Down = false;

// handle underline font button
if(Memo->SelAttributes->Style.Contains(fsUnderline))
 FontUnderBtn->Down = true;
else
 FontUnderBtn->Down = false;

 PopupCopy->Enabled = false;
 PopupCut->Enabled = false;
 }

// handle bold font button
if(Memo->SelAttributes->Style.Contains(fsBold))
 FontBoldBtn->Down = true;
else
 FontBoldBtn->Down = false;

// handle italic font button
if(Memo->SelAttri                                                                                                                                                                         false;
 EditCopy->Enabled = false;
 EditCut->Enabled = false;                                                                                                                                                                         cut and copy
 EditCopyBtn->Enabled = true;
 EditCutBtn->Enabl                                                                                                                                                                     -------------------------------------------------------

void                                                                                                                                                                                    == IDCANCEL) return; // do nothing
 }
// Open file will take c                                                                                                                                                                     nt file needs to be saved
if(Memo->Modified)
 {
 int result =                                                                                                                                                                               ender)
{
// Load a file in the Memo component based on
// the                                                                                                                                                                        __fastcall TScratchPad::FilePrintSetupClick(TObject *Sender)
{                                                                                                                                                                               ------

void __fastcall TScratchPad::FilePrintClick(TObject *S                                                                                                                                                                         ialog->FilterIndex); // save file extension
 // we ought to adj                                                                                                                                                                            Name);

 // now we're going to save the path for the last save                                                                                                                                                                         // save as a rich text file

 Memo->Lines->SaveToFile(SaveDia                                                                                                                                                                                  s of Memo
SaveDialog->Title = "Save As";
if(SaveDialog->Execut                                                                                                                                                                     
{
// load info for save as.. box from registry
try
 {
 Save                                                                                                                                                                                ms[MOD_PANEL]->Text = "Unmodified";
 }
else FileSaveAsClick(Se                                                                                                                                                                     xt(SaveDialog->FileName) != ".rtf")
  Memo->PlainText = true; /                                                                                                                                                                        tchPad::FileSaveClick(TObject *Sender)
{
// Display SaveDialog                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ¿ÿÿÿ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           BM>       >   (   @   0                               ÿÿÿ ÿÿÿ                             ¿ÿÿÿ         ÿÿÿ   ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ ÿÿÿ¿ÿÿÿ                            ÿÿÿÿÿÿ  ÿÿÿÿÿÿ  ÿÿÿÿÿ  ÀÀ  ÿÿÿÿÿ  ø?øÿÿ  ÿÿÿÿÿÿ  ÀÀÿ  ÿÿÿÿ  ðøÿ  ÿÿÿÿÿÿ  ÀÀÿÿ  ÿÿÿÿÿ  ø?ø  ÿÿÿÿÿ  ÿÿÿÿÿÿ  ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿàÿÿÿÿÿÿÿÿÀðÿø?ÿÿøÇãÿóÀ?øÇñÿóÿÿøÇøÿóøüóÿÿøÇþ?óÀ?øÇÿóÿÿøÇÿóÀðÿáÿÿÿÿÿÿÿÿÀ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ                        TFindDialog
FindDialogOnFindFindDialogFindLeftÈ Top  TReplaceDialogReplaceDialogOnFindFindDialogFind	OnReplaceReplaceDialogReplaceLeftè Top  
TPopupMenu	PopupViewLefthTop 	TMenuItem	PopupTBarCaptionToolbarChecked	OnClickViewToolbarClick  	TMenuItem	PopupFBarCaption
Format BarChecked	OnClickViewFormatBarClick  	TMenuItem	PopupSBarCaption
Status BarChecked	OnClickViewStatusBarClick    