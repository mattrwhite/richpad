//----------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "RFBoxMain.h"
#include "RichMain.h"
//----------------------------------------------------------------------------
#pragma resource "*.dfm"
TReformatBox *ReformatBox;
//----------------------------------------------------------------------------
__fastcall TReformatBox::TReformatBox(TComponent *Owner)
	: TForm(Owner)
{
}
//----------------------------------------------------------------------------
void __fastcall TReformatBox::RFBoxCancelClick(TObject *Sender)
{
ReformatBox->Close();    
}
//---------------------------------------------------------------------------
void __fastcall TReformatBox::RFBoxOKClick(TObject *Sender)
{
// the order we do these does matter since RemoveTabs won't work properly if
//  some lines have been combined
// We need to set these up to act only on selected text!
// Implementation: SendMsg EM_LINEFROMCHAR for SelStart (a) and SelStart +
//  SelLength (b) - iterate between Strings[a] and Strings[b]
// lets setup bounds first: do defaults
int RFStart = 0; // starting line number
int RFEnd = ScratchPad->Memo->Lines->Count; // ending line number

// Fix RemoveCRLF stuff

if(ScratchPad->Memo->SelLength > 0)
 {
 // get zero based line number for SelStart
 RFStart = SendMessage(ScratchPad->Memo->Handle,
  EM_LINEFROMCHAR,ScratchPad->Memo->SelStart,0);
 // RFEnd is a little more complex.  If a selection is made via the left margin,
 //  the end of it will be the first char of the next line, which we don't want;
 //  anyway, by subtracting 1 we fix this, while adding one to the final result
 //  is also necessary
 RFEnd = SendMessage(ScratchPad->Memo->Handle,
  EM_LINEFROMCHAR,ScratchPad->Memo->SelStart +
   ScratchPad->Memo->SelLength - 1,0) + 1;
 }
if(RemoveTabs->Checked)
 {
 // for each Memo->Lines[n], remove all leading white space
 // this will probably only work in plain text!
 for(int i = RFStart; i < RFEnd; i++)
  {
  ScratchPad->Memo->Lines->Strings[i] =
   ScratchPad->Memo->Lines->Strings[i].TrimLeft(); // could it be any easier?
  }
 }
if(RemoveCRLF->Checked)
 {
 // recreate Memo->Lines->Text from Memo->Lines->Strings[0..n]
 // Works great - and it re-word-wraps
 AnsiString* NewText = new AnsiString;
 // new solution: create NewText normally, then delete the affected strings and
 //  insert NewText as a single line!
 for(int i = RFStart; i < RFEnd; i++)
  {
  // add a space after each line being combined
  *NewText += ScratchPad->Memo->Lines->Strings[RFStart] + " ";
  ScratchPad->Memo->Lines->Delete(RFStart);
  // now all the other strings will move up one
  }
 ScratchPad->Memo->Lines->Insert(RFStart, *NewText);
 delete NewText;
 }
}
//---------------------------------------------------------------------------
void __fastcall TReformatBox::FormShow(TObject *Sender)
{
// set caption based on SelLength
if(ScratchPad->Memo->SelLength > 0)
 ReformatBox->Caption = "Reformat Selection";
else
 ReformatBox->Caption = "Reformat Document";
}
//---------------------------------------------------------------------------

