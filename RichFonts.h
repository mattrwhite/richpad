//---------------------------------------------------------------------------
#ifndef RichFontsH
#define RichFontsH
//---------------------------------------------------------------------------
#include <Registry.hpp>

// we're going to create a class for keeping track of fonts
// This class will ba able to save and set the properties
//  of font-related objects, and store that information
//  in the registry

class TRichFonts
{
public:
 // SaveFont writes the font information to the registry
 bool SaveFont(TRegistry* SaveKey, AnsiString SValue);
 bool RestoreFont(TRegistry* RestoreKey, AnsiString RValue);
 // these functions store the properties of a TFont or
 //  TTextAttributes object in the TRichFonts object
 bool StoreFont(TFont* SFont);
 bool StoreFont(TTextAttributes* SAttrib);
 // these functions set the properties of a TFont or
 //  TTextAttributes object from saved data
 bool SetFont(TFont* SFont);
 bool SetFont(TTextAttributes* SAttrib);
 // options flags
 bool SaveFontStyle;
 bool SaveFontColor;
private:
 // font data
 Byte Charset;
 TColor Color;     // should have
 AnsiString Name;  // must have
 int Size;         // must have
 TFontStyles FontStyle;
};

struct tagSaveFont
{
Byte sCharset;
int sColor;
char sName[32];
int sSize;
bool sBold;
bool sItalic;
bool sUnderline;
bool sStrikeOut;
};

#endif

