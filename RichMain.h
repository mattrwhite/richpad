//---------------------------------------------------------------------------
#ifndef RichMainH
#define RichMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <ToolWin.hpp>
#include <Registry.hpp>
#include <vcl\Clipbrd.hpp>
#include "Options.h"
#include "RichFonts.h"
#include <ImgList.hpp>
//---------------------------------------------------------------------------
class TScratchPad : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar;
        TMainMenu *MainMenu;
        TMenuItem *FileMenu;
        TMenuItem *FileNew;
        TMenuItem *FileOpen;
        TMenuItem *FileSave;
        TMenuItem *FileSaveAs;
        TMenuItem *N1;
        TMenuItem *FilePrint;
        TMenuItem *FilePrintSetup;
        TMenuItem *MRUTop;
        TMenuItem *FileExit;
        TMenuItem *Edit1;
        TMenuItem *EditReplace;
        TMenuItem *EditFind;
        TMenuItem *N4;
        TMenuItem *EditPaste;
        TMenuItem *EditCopy;
        TMenuItem *EditCut;
        TMenuItem *N5;
        TMenuItem *EditUndo;
        TMenuItem *Help1;
        TMenuItem *HelpAbout;
        TMenuItem *HelpContents;
        TMenuItem *EditSelectAll;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
    TCoolBar *CoolBar;
    TToolBar *MainToolBar;
    TToolButton *FileNewBtn;
    TToolButton *FileOpenBtn;
    TToolButton *FileSaveBtn;
    TToolButton *ToolButton1;
    TToolButton *EditCutBtn;
    TToolButton *EditCopyBtn;
    TToolButton *EditPasteBtn;
        TRichEdit *Memo;
        TPopupMenu *MemoPopup;
        TMenuItem *PopupUndo;
        TMenuItem *N3;
        TMenuItem *PopupCut;
        TMenuItem *PopupCopy;
        TMenuItem *PopupPaste;
        TMenuItem *PopupSelectAll;
        TMenuItem *N6;
    TFontDialog *FontDialog;
        TPrinterSetupDialog *PrinterSetupDialog;
        TPrintDialog *PrintDialog;
        TToolBar *FormatBar;
        TMenuItem *View1;
        TMenuItem *ViewToolbar;
        TMenuItem *ViewFormatBar;
        TComboBox *FontBox;
        TComboBox *SizeBox;
        TToolButton *FilePrintBtn;
        TToolButton *SearchBtn;
        TToolButton *UndoBtn;
        TImageList *ImageList1;
        TToolButton *ToolButton2;
        TImageList *ImageList2;
        TToolButton *FontBoldBtn;
        TToolButton *FontItalicBtn;
        TToolButton *FontUnderBtn;
        TToolButton *AlignLeftBtn;
        TToolButton *AlignCenterBtn;
        TToolButton *AlignRightBtn;
        TToolButton *BulletBtn;
        TToolButton *ToolButton11;
        TToolButton *ToolButton12;
        TToolButton *ToolButton13;
        TFindDialog *FindDialog;
        TReplaceDialog *ReplaceDialog;
        TMenuItem *ViewStatusBar;
        TMenuItem *Format1;
        TMenuItem *FormatSelectFont;
        TMenuItem *FormatWordWrap;
        TMenuItem *FormatBullets;
        TMenuItem *FormatDefaultFont;
        TMenuItem *MRUBottom;
    TMenuItem *ViewOptions;
    TPanel *PopupEdit;
    TMenuItem *EditFindNext;
    TPopupMenu *PopupView;
    TMenuItem *PopupTBar;
    TMenuItem *PopupFBar;
    TMenuItem *PopupSBar;
    TMenuItem *FormatBarShortcuts1;
    TMenuItem *Bold1;
    TMenuItem *Italic1;
    TMenuItem *Underline1;
    TMenuItem *LeftJustify1;
    TMenuItem *Center1;
    TMenuItem *RightJustify1;
    TMenuItem *FormatReformat;
        void __fastcall FileExitClick(TObject *Sender);
        void __fastcall FileSaveAsClick(TObject *Sender);
        void __fastcall FileSaveClick(TObject *Sender);
        void __fastcall FileOpenClick(TObject *Sender);
        void __fastcall FileNewClick(TObject *Sender);
        void __fastcall EditUndoClick(TObject *Sender);
        void __fastcall EditCutClick(TObject *Sender);
        void __fastcall EditCopyClick(TObject *Sender);
        void __fastcall EditPasteClick(TObject *Sender);
        void __fastcall EditSelectAllClick(TObject *Sender);
        void __fastcall EditWordWrapClick(TObject *Sender);
        void __fastcall HelpAboutClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall MemoChange(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);

        void __fastcall MemoSelectionChange(TObject *Sender);
    void __fastcall SelectFontClick(TObject *Sender);
        
        void __fastcall FilePrintClick(TObject *Sender);
        void __fastcall FilePrintSetupClick(TObject *Sender);
        void __fastcall ViewToolbarClick(TObject *Sender);
        void __fastcall ViewFormatBarClick(TObject *Sender);
        
        void __fastcall FontBoxClick(TObject *Sender);
        void __fastcall SizeBoxKeyPress(TObject *Sender, char &Key);
        void __fastcall SizeBoxClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FilePrintBtnClick(TObject *Sender);
    void __fastcall FontBoldBtnClick(TObject *Sender);
    void __fastcall FontItalicBtnClick(TObject *Sender);
    void __fastcall FontUnderBtnClick(TObject *Sender);

    void __fastcall AlignLeftBtnClick(TObject *Sender);
    void __fastcall AlignCenterBtnClick(TObject *Sender);
    void __fastcall AlignRightBtnClick(TObject *Sender);
        void __fastcall EditFindClick(TObject *Sender);
        void __fastcall EditReplaceClick(TObject *Sender);
        void __fastcall FindDialogFind(TObject *Sender);
        void __fastcall ReplaceDialogReplace(TObject *Sender);
        void __fastcall BulletBtnClick(TObject *Sender);
        void __fastcall ViewStatusBarClick(TObject *Sender);
        void __fastcall FormatBulletsClick(TObject *Sender);
        void __fastcall FormatDefaultFontClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall MRUClick(TObject *Sender);
    void __fastcall ViewOptionsClick(TObject *Sender);
    void __fastcall FontBoxDrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
    void __fastcall FontBoxDropDown(TObject *Sender);
    void __fastcall FormatReformatClick(TObject *Sender);
private:	// User declarations
        void __fastcall OnHint(TObject* Sender);
        void __fastcall OnActivate(TObject* Sender);
        // open the passed file
        bool OpenFile(AnsiString OFile);
        // function to draw hist. list
        void DrawHistory(void);
        void AddToHistory(AnsiString FileToAdd);
        TStringList* MruList; // pointer for history list
        RichPadOptions* CurrOptions;
        TRect PreviewBox;
        TRegistry* RichPadKey; // just use one key!
        // this method sets Memo->Modified to false, etc.
        void ClearModified(bool FileOpened);
// number of files to keep in history
// note that if MruCount is changed, we must delete the
//  MRUList string value!!!
        int MruCount;
        // graphics object for TrueType bitmap
        Graphics::TBitmap* TTFont;
public:		// User declarations
        __fastcall TScratchPad(TComponent* Owner);
        void ClearHistory();
        void DeleteKey();
        // get and set options fuctions
        RichPadOptions* GetOptions();
        void SetOptions(RichPadOptions* NewOptions);
        void RefreshFont();
        void ResetDefFont();
        // default font stuff - consider using an array
        // great, except for the registry aspect
        TRichFonts* NewFont;
        TRichFonts* TXTFont;
        TRichFonts* OtherFont;
};
//---------------------------------------------------------------------------
extern PACKAGE TScratchPad *ScratchPad;
//---------------------------------------------------------------------------
#endif
