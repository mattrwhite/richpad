//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "RichMain.h"
#include "About.h"
#include "RFBoxMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

// defines for Status Bar panels
#define HINT_PANEL 0
#define POS_PANEL 1
#define MOD_PANEL 2
// Max length for font preview string
#define MAX_PREVIEW_LENGTH 20

// registry stuff
const char* RegKey = "Software\\Matt\\RichPad";
const char* DefNewFont = "DefNewFont"; // simple enough
const char* DefTXTFont = "DefTXTFont";
const char* DefOtherFont = "DefOtherFont";
// Default text for font preview
char PreviewText[] = "AaBbYyZz";

TScratchPad *ScratchPad;
//---------------------------------------------------------------------------
__fastcall TScratchPad::TScratchPad(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
// OnActivate, OnHint - event handlers for ScratchPad Application
void __fastcall TScratchPad::OnActivate(TObject *Sender)
{
// this handler will see if we need to enable edit paste
// check anytime the user switches to ScratchPad
if(Clipboard()->HasFormat(CF_TEXT))
 {
 EditPasteBtn->Enabled = true;
 EditPaste->Enabled = true;
 PopupPaste->Enabled = true;
 }
else
 {
 EditPasteBtn->Enabled = false;
 EditPaste->Enabled = false;
 PopupPaste->Enabled = false;
 }
// do we need to disable paste after we paste???
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::OnHint(TObject* Sender)
{
StatusBar->Panels->Items[HINT_PANEL]->Text = Application->Hint;
}

// EnumProc - Windows calls this proc once for each font it
//  finds
// passed: fontname, etc., data is the ptr to Label1

int CALLBACK EnumProc(LOGFONT *logfont, TEXTMETRIC *textmetric, DWORD type, LPARAM data)
{
TComboBox* cb = (TComboBox*)data;
cb->Items->Add(String(logfont->lfFaceName));
return 1;
}
//------------------------------------------------------------------

// FormCreate, FormDestroy, FormCloseQuery, FormShow
//  - event handlers for ScratchPad form

void __fastcall TScratchPad::FormCreate(TObject *Sender)
{
// set event handlers for Application object
Application->OnHint = &OnHint;
Application->OnActivate = &OnActivate;

// next, setup the font selection box
// call Windows to get a list of fonts
// we're going to try using EnumFontFamiliesEx
LOGFONT FontInfo;
// zero out FontInfo
memset(&FontInfo, 0, sizeof(LOGFONT));
FontInfo.lfCharSet = ANSI_CHARSET; // enum from Ansi charset
EnumFontFamiliesEx(Canvas->Handle, &FontInfo, (FONTENUMPROC)EnumProc, (LPARAM)FontBox, 0);
// setup font and size combo boxes
FontBox->ItemIndex = FontBox->Items->IndexOf(Memo->Font->Name);
SizeBox->ItemIndex = SizeBox->Items->IndexOf(Memo->Font->Size);

// load resources
// - bitmap for TrueType Fonts
TTFont = new Graphics::TBitmap;
TTFont->LoadFromResourceName((int)HInstance, "TTFONT");

// Load options from registry or set defaults
// - allocate RichPad's TRegistry object
RichPadKey = new TRegistry; // this will be used throughout
RichPadKey->OpenKey(RegKey,true);
// get all our options first
CurrOptions = new RichPadOptions;
// load options from registry
if(!RichPadKey->ReadBinaryData("Options", CurrOptions,
 sizeof(RichPadOptions)))
 {
 // set default options
 CurrOptions->FullPath = true;
 CurrOptions->InFileMenu = true; // unused
 CurrOptions->HistItems = 5;
 CurrOptions->NewFile.WordWrap = true;
 CurrOptions->NewFile.ShowToolbar = true;
 CurrOptions->NewFile.ShowFBar = true;
 CurrOptions->NewFile.ShowStatBar = true;
 CurrOptions->TXTFile.WordWrap = true;
 CurrOptions->TXTFile.ShowToolbar = true;
 CurrOptions->TXTFile.ShowFBar = true;
 CurrOptions->TXTFile.ShowStatBar = true;
 CurrOptions->OtherFile.WordWrap = true;
 CurrOptions->OtherFile.ShowToolbar = true;
 CurrOptions->OtherFile.ShowFBar = true;
 CurrOptions->OtherFile.ShowStatBar = true;
 }

// next, handle the File history stuff, here's how:
// Whenever a file is opened, we add it to the history list
//  and the list is refreshed whenever the current file is
//  closed, or when the form is created - as follows
MruList = new TStringList;
if(!RichPadKey->ValueExists("MRUList"))
 {
 for (int i=0;i<MruCount;i++) // clear the StringList
  MruList->Add("");
 RichPadKey->WriteString("MRUList", MruList->Text);
 }
// Load MruList from existing registry entry
MruList->Text = RichPadKey->ReadString("MRUList");

// TRichFont structs for default fonts for different files
NewFont = new TRichFonts; // def. font for new files
TXTFont = new TRichFonts; // def. font for .txt files
OtherFont = new TRichFonts; // def. font for other files

// load the def. font structs from the registry
// on failure, we set the font to Memo->Font
if(!NewFont->RestoreFont(RichPadKey,DefNewFont))
 NewFont->StoreFont(Memo->Font);
if(!TXTFont->RestoreFont(RichPadKey,DefTXTFont))
 TXTFont->StoreFont(Memo->Font);
if(!OtherFont->RestoreFont(RichPadKey,DefOtherFont))
 OtherFont->StoreFont(Memo->Font);

// make necessary changes to settings, draw history,
//  and refresh Memo->Font and Memo->Wordwrap
SetOptions(CurrOptions);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FormDestroy(TObject *Sender)
{
// add the last file that was opened to the history list
AddToHistory(SaveDialog->FileName); // hope this is still accesible
// Save the history list to the registry
RichPadKey->WriteString("MRUList", MruList->Text);
// now write our option settings to the registry
RichPadKey->WriteBinaryData("Options", CurrOptions,
 sizeof(RichPadOptions));
// Save default font stuff
NewFont->SaveFont(RichPadKey,DefNewFont);
TXTFont->SaveFont(RichPadKey,DefTXTFont);
OtherFont->SaveFont(RichPadKey,DefOtherFont);
// cleanup
delete NewFont;
delete TXTFont;
delete OtherFont;
// Clean up the TRegistry object and the MRU list.
delete RichPadKey; // delete RichPad's key and save changes
delete MruList;
delete TTFont; // delete TrueType font bitmap
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
// Before closing, prompt to save current file
if(Memo->Modified)
 {
 CanClose = false; // default is don't close
 int result = Application->MessageBox(
     "The current file has changed. Save Changes?",
      "RichPad Message", MB_YESNOCANCEL | MB_ICONQUESTION);
 if(result == IDYES)
  {
  FileSaveClick(0);
  // if the user didn't save the file, don't close
  if(Memo->Modified) return; // still modified?
  }
 if(result == IDCANCEL) return;
 }
CanClose = true;
// Fall through if result is IDNO or after saving file
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FormShow(TObject *Sender)
{
// Take care of opening the passed file last so it doesn't
//  get included in the history list!!!
// now handle the command line parameters - open passed file
if(ParamCount() == 1) // only accept 1 parameter
 OpenFile(ParamStr(1));
// if a long file name with spaces was passed without quotes
// - Windows does this! - combine the strings
else if(ParamCount() > 1)
 {
 AnsiString fcFileName;
 for(int i = 1; i <= ParamCount(); i++)
  {
  if(ParamStr(i).IsDelimiter("/-", 0)) // in case we have command line options
   break;
  fcFileName += ParamStr(i) + " ";
  }
 // delete the trailing space and open the file
 fcFileName.Delete(fcFileName.Length(), 1);
 OpenFile(fcFileName);
 }
else // if no file opened
 ClearModified(false); // restore Memo to mint condition
// when the form is shown, make sure Memo gets focus
ScratchPad->FocusControl(Memo);
}
//---------------------------------------------------------------------------

// MemoChange, MemoSelectionChange - event handlers for RichEdit component

void __fastcall TScratchPad::MemoChange(TObject *Sender)
{
// enable undo and search items
SearchBtn->Enabled = true;
UndoBtn->Enabled = true;
EditFind->Enabled = true;
EditFindNext->Enabled = true;
EditReplace->Enabled = true;
EditUndo->Enabled = true;
// enable the proper menu / toolbar items
FileSaveAs->Enabled = true;
FileNewBtn->Enabled = true;
FileNew->Enabled = true;
 // enable select all
EditSelectAll->Enabled = true;
PopupSelectAll->Enabled = true;
// do file save (vs. save as buttons)
FileSaveBtn->Enabled = true;
FileSave->Enabled = true;
Memo->Modified = true; // since we messed with it before
StatusBar->Panels->Items[MOD_PANEL]->Text = "Modified";
}
//---------------------------------------------------------------------------

// ClearModified - set up RichPad as if Memo was never
//  modified.  If FileOpened = true, we need to enable
// new, print, search, etc. since Memo contains text.

void TScratchPad::ClearModified(bool FileOpened)
{
FileSaveBtn->Enabled = false;
FileSave->Enabled = false;
if(!FileOpened)
 {
 // disable items enabled in MemoChange
 SearchBtn->Enabled = false;
 UndoBtn->Enabled = false;
 EditFind->Enabled = false;
 EditFindNext->Enabled = false;
 EditReplace->Enabled = false;
 EditUndo->Enabled = false;
 // disable the proper menu / toolbar items
 FileSaveAs->Enabled = false;
 FileNewBtn->Enabled = false;
 FileNew->Enabled = false;
 // disable select all
 EditSelectAll->Enabled = false;
 PopupSelectAll->Enabled = false;
 }
// change status bar and Memo->Modified
StatusBar->Panels->Items[MOD_PANEL]->Text = "Unmodified";
Memo->Modified = false;
}

void __fastcall TScratchPad::MemoSelectionChange(TObject *Sender)
{
// this method handles command enabling, etc.
if(Memo->SelLength > 0)
 {
 // if text is selected, enable cut and copy
 EditCopyBtn->Enabled = true;
 EditCutBtn->Enabled = true;
 EditCopy->Enabled = true;
 EditCut->Enabled = true;
 PopupCopy->Enabled = true;
 PopupCut->Enabled = true;
 }
else
 {
 EditCopyBtn->Enabled = false;
 EditCutBtn->Enabled = false;
 EditCopy->Enabled = false;
 EditCut->Enabled = false;
 PopupCopy->Enabled = false;
 PopupCut->Enabled = false;
 }

// handle bold font button
if(Memo->SelAttributes->Style.Contains(fsBold))
 FontBoldBtn->Down = true;
else
 FontBoldBtn->Down = false;

// handle italic font button
if(Memo->SelAttributes->Style.Contains(fsItalic))
 FontItalicBtn->Down = true;
else
 FontItalicBtn->Down = false;

// handle underline font button
if(Memo->SelAttributes->Style.Contains(fsUnderline))
 FontUnderBtn->Down = true;
else
 FontUnderBtn->Down = false;

 // handle justification buttons
if(Memo->Paragraph->Alignment == taLeftJustify)
 AlignLeftBtn->Down = true;
else if(Memo->Paragraph->Alignment == taCenter)
 AlignCenterBtn->Down = true;
else if(Memo->Paragraph->Alignment == taRightJustify)
 AlignRightBtn->Down = true;

// handle font type and size combo boxes
FontBox->ItemIndex = FontBox->Items->IndexOf(Memo->SelAttributes->Name);
SizeBox->ItemIndex = SizeBox->Items->IndexOf(Memo->SelAttributes->Size);

// handle bullet menu item / button
if(Memo->Paragraph->Numbering == nsBullet)
 {
 BulletBtn->Down = true;
 FormatBullets->Checked = true;
 }
else
 {
 BulletBtn->Down = false;
 FormatBullets->Checked = false;
 }
// finally, handle the line and position number in the status bar
int line = SendMessage(Memo->Handle,EM_LINEFROMCHAR,Memo->SelStart,0);
int pos = SendMessage(Memo->Handle,EM_LINEINDEX,line,0);
StatusBar->Panels->Items[POS_PANEL]->Text = String("Line ") +
 String(line + 1) + String("; Pos. ") + String(Memo->SelStart - pos + 1);
}
//---------------------------------------------------------------------------

// Event handlers for Menus

//  File Menu event handlers
//---------------------------------------------------------------------------

// FileNewClick uses my form of command enabling. I prefer
//  to handle enabling only when neccessary instead of
//  through the OnIdle handler - speed over elegance.

void __fastcall TScratchPad::FileNewClick(TObject *Sender)
{
// create a new file after seeing if current one needs to be saved
if(Memo->Modified)
 {
 int result = Application->MessageBox(
     "The current file has changed. Save Changes?",
     "RichPad Message", MB_YESNOCANCEL | MB_ICONQUESTION);
 if(result == IDYES) FileSaveClick(Sender);
 if(result == IDCANCEL) return; // do nothing
 }
if(Memo->Lines->Count > 0)
 Memo->Clear(); //clears Memo->Modified
// add the last file opened to the history list
AddToHistory(SaveDialog->FileName);
DrawHistory(); // redraw
SaveDialog->FileName = "";
// finally, clear the caption and draw the history list
ScratchPad->Caption = "RichPad - Untitled";
RefreshFont(); // set Memo->Font to NewFont
// Refresh font will set modified
ClearModified(false);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FileOpenClick(TObject *Sender)
{
// Open a new file after seeing if current file needs to be saved
if(Memo->Modified)
 {
 int result = Application->MessageBox(
     "The current file has changed. Save Changes?",
      "RichPad Message", MB_YESNOCANCEL | MB_ICONQUESTION);
 if(result == IDYES) FileSaveClick(0);
 if(result == IDCANCEL) return; // do nothing
 }
// Setup and execute File Open dialog
try
 {
 OpenDialog->InitialDir = RichPadKey->ReadString("OpenDir");
 OpenDialog->FileName = RichPadKey->ReadString("OpenFile");
 OpenDialog->FilterIndex = RichPadKey->ReadInteger("OpenIndex");
 }
catch(...)
 {
 OpenDialog->FileName = ""; // if it didn't work, set default
 }

// Here's the problem: when we open a file in the TRichEdit,
//  it thinks the file has been modified, so we use
//  ClearModified()
if(OpenDialog->Execute())
 {
 if(OpenFile(OpenDialog->FileName))
  {
  // now we're going to save info for the last file opened
  //  to the registry - RichPadKey already exists
  RichPadKey->WriteString("OpenDir",
   ExtractFilePath(OpenDialog->FileName)); // save path
  RichPadKey->WriteString("OpenFile",
   ExtractFileName(OpenDialog->FileName)); // save name
  RichPadKey->WriteInteger("OpenIndex",
   OpenDialog->FilterIndex); // save file extension
  }
 }
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FileSaveClick(TObject *Sender)
{
// Display SaveDialog Box to save contents of Memo
if(SaveDialog->FileName != "")
 {
 // decide how we should save the file
 // Note that SaveDialog->FileName is set whenever a file is opened
 if(ExtractFileExt(SaveDialog->FileName) != ".rtf")
  Memo->PlainText = true; // save as a plain text file
 else
  Memo->PlainText = false; // save as a rich text file

 Memo->Lines->SaveToFile(SaveDialog->FileName);
 Memo->Modified = false;
 StatusBar->Panels->Items[MOD_PANEL]->Text = "Unmodified";
 }
else FileSaveAsClick(Sender);  // default to Save As if no filename
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FileSaveAsClick(TObject *Sender)
{
// load info for save as.. box from registry
try
 {
 SaveDialog->InitialDir = RichPadKey->ReadString("SaveDir");
 SaveDialog->FilterIndex = RichPadKey->ReadInteger("SaveIndex");
 }
catch(...) {}

// Display Save As... dialog box to save contents of Memo
SaveDialog->Title = "Save As";
if(SaveDialog->Execute())
 {
// see what we should save the file as
 if(ExtractFileExt(SaveDialog->FileName) != ".rtf")
  Memo->PlainText = true; // save as a plain text file
 else
  Memo->PlainText = false; // save as a rich text file

 Memo->Lines->SaveToFile(SaveDialog->FileName);
 Memo->Modified = false;
 StatusBar->Panels->Items[MOD_PANEL]->Text = "Unmodified";
 // add title of file to caption
 ScratchPad->Caption = ("RichPad - " + SaveDialog->FileName);

 // now we're going to save the path for the last save in
 //  the registry - RichPadKey is ready to go
 RichPadKey->WriteString("SaveDir",
  ExtractFilePath(SaveDialog->FileName)); // save path
 RichPadKey->WriteInteger("SaveIndex",
  SaveDialog->FilterIndex); // save file extension
 // we ought to adjust font based on new file name
 RefreshFont();
 ClearModified(true); // should work, not very elegant, though
 }
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FilePrintClick(TObject *Sender)
{
if(PrintDialog->Execute())
 Memo->Print(ScratchPad->Caption); // use file name as caption
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FilePrintSetupClick(TObject *Sender)
{
PrinterSetupDialog->Execute(); // simply run setup dialog
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::MRUClick(TObject *Sender)
{
// Load a file in the Memo component based on
// the MRU item that was clicked.
TMenuItem* itemClicked = dynamic_cast<TMenuItem*>(Sender);
int index = itemClicked->MenuIndex - MRUTop->MenuIndex - 1;

// Open a new file after seeing if current file needs to be saved
if(Memo->Modified)
 {
 int result = Application->MessageBox(
     "The current file has changed. Save Changes?",
      "RichPad Message", MB_YESNOCANCEL | MB_ICONQUESTION);
 if(result == IDYES) FileSaveClick(0);
 if(result == IDCANCEL) return; // do nothing
 }
// Open file will take care of everything (hist. list, etc.)
if(!OpenFile(MruList->Strings[index]))
 {
 // open file failed
 MruList->Delete(index);
 DrawHistory(); // show changes
 }
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FileExitClick(TObject *Sender)
{
// We don't need to prompt for saving - OnCloseQuery handles it
Close();
}
//---------------------------------------------------------------------------

//  Edit menu event handlers

void __fastcall TScratchPad::EditUndoClick(TObject *Sender)
{
// TMemo has no Undo method so use windows WM_UNDO message
SendMessage(Memo->Handle, WM_UNDO, 0, 0);
// we could implement a toggle between undo and redo
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::EditCutClick(TObject *Sender)
{
Memo->CutToClipboard();
EditPasteBtn->Enabled = true;
EditPaste->Enabled = true;
PopupPaste->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::EditCopyClick(TObject *Sender)
{
Memo->CopyToClipboard();
EditPasteBtn->Enabled = true;
EditPaste->Enabled = true;
PopupPaste->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::EditPasteClick(TObject *Sender)
{
Memo->PasteFromClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::EditSelectAllClick(TObject *Sender)
{
Memo->SelectAll();
// must enable cut and copy
EditCutBtn->Enabled = true;
EditCopyBtn->Enabled = true;
EditCut->Enabled = true;
EditCopy->Enabled = true;
PopupCut->Enabled = true;
PopupCopy->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::EditWordWrapClick(TObject *Sender)
{
// Toggle Format:WordWarp
Memo->WordWrap = !Memo->WordWrap;
FormatWordWrap->Checked = Memo->WordWrap;
// if WordWrap is off, we have to create a horz. scroll bar
if(Memo->WordWrap) Memo->ScrollBars = ssVertical;
else Memo->ScrollBars = ssBoth;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::EditFindClick(TObject *Sender)
{
FindDialog->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::EditReplaceClick(TObject *Sender)
{
ReplaceDialog->Execute();
}
//---------------------------------------------------------------------------
// Event handlers for find / replace dialogs

void __fastcall TScratchPad::FindDialogFind(TObject *Sender)
{
// When the user clicks on Find, this method gets called
// create pointer to FindDialog to access properties
TFindDialog* FDialog = (TFindDialog*)Sender;
int pos;
// read search options from FindDialog
TSearchTypes options;
if(FDialog->Options.Contains(frMatchCase))
 options << stMatchCase;
if(FDialog->Options.Contains(frWholeWord))
 options << stWholeWord;
// now search up or down
if(FDialog->Options.Contains(frDown))
 {
 int start = Memo->SelStart;
 if (Memo->SelLength != 0)
  ++start;
 pos = Memo->FindText(FDialog->FindText, start,
  Memo->Text.Length() - Memo->SelStart - 1, options);
 }
else if(Memo->SelStart > 0)
 {
 int search = -1;
 do
  {
  pos = search;
  search = Memo->FindText(FDialog->FindText, search + 1,
   Memo->SelStart - search - 1, options);
  } while(search >= 0);
 }
else
 pos = -1;

// now that we've found the text, select it
if(pos >= 0)
 {
 Memo->SelStart = pos;
 Memo->SelLength = FDialog->FindText.Length();
 // finally, bring the main form to the front
 ScratchPad->BringToFront();
 }
else
{
// if we didn't find the text, display a message box
 Application->MessageBox(
     "RichPad has finished searching",
      "RichPad Message", MB_OK | MB_ICONINFORMATION);
}
}

//---------------------------------------------------------------------------

void __fastcall TScratchPad::ReplaceDialogReplace(TObject *Sender)
{
if(ReplaceDialog->Options.Contains(frReplaceAll))
 {
 TSearchTypes options;
 if(ReplaceDialog->Options.Contains(frMatchCase))
  options << stMatchCase;
 if(ReplaceDialog->Options.Contains(frWholeWord))
  options << stWholeWord;
 int SearchLength = Memo->Text.Length();
// should we start searching from pos 0 or the cursor?
 int pos = Memo->FindText(ReplaceDialog->FindText, 0, SearchLength, options);
 while(pos >= 0)
  {
  Memo->SelStart = pos;
  Memo->SelLength = ReplaceDialog->FindText.Length();
  // set the selected text to replace
  Memo->SelText = ReplaceDialog->ReplaceText;
  // oops! Need to start searching from pos + FindText.Length!
  // note that replacing SelText resets SelLength!
  pos += ReplaceDialog->ReplaceText.Length();
  pos = Memo->FindText(ReplaceDialog->FindText, pos, SearchLength, options);
  }
 }
else if(Memo->SelLength > 0)
 Memo->SelText = ReplaceDialog->ReplaceText;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::HelpAboutClick(TObject *Sender)
{
AboutBox->ShowModal();
}
//---------------------------------------------------------------------------
//  Format Menu event handlers

void __fastcall TScratchPad::SelectFontClick(TObject *Sender)
{
// set the font dialog box to the font of the selected text
FontDialog->Font->Assign(Memo->SelAttributes);
// set Memo selection font to font selected in dialog box
if(FontDialog->Execute())
 {
 Memo->SelAttributes->Assign(FontDialog->Font);
 MemoSelectionChange(Sender); // this will update the ComboBoxes
 }
// if no text is selected, all text typed after changing
//  the font appears in the new font!!!
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FormatBulletsClick(TObject *Sender)
{
FormatBullets->Checked = !FormatBullets->Checked;
// update the BulletBtn toolbutton
BulletBtn->Down = FormatBullets->Checked;
if(FormatBullets->Checked)
 Memo->Paragraph->Numbering = nsBullet;
else
 Memo->Paragraph->Numbering = nsNone;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FormatDefaultFontClick(TObject *Sender)
{
// similar to Select Font, but changes Memo->Font
// saves the default font via TRichFonts
FontDialog->Font->Assign(Memo->Font);
// set Memo->Font to font selected in dialog box
if(FontDialog->Execute())
 {
 Memo->Font->Assign(FontDialog->Font);
 MemoSelectionChange(Sender); // update fontbox
 }
}
//---------------------------------------------------------------------------

// handler for print on main toolbar
void __fastcall TScratchPad::FilePrintBtnClick(TObject *Sender)
{
// print the document immediatly
Memo->Print(ScratchPad->Caption); // use file name as caption
}
//---------------------------------------------------------------------------

// View menu event handlers

void __fastcall TScratchPad::ViewToolbarClick(TObject *Sender)
{
// Hide / show main toolbar (CoolBar->Bands->Items[0])
ViewToolbar->Checked = !ViewToolbar->Checked; // toggle
PopupTBar->Checked = !PopupTBar->Checked;
CoolBar->Bands->Items[0]->Visible = ViewToolbar->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::ViewFormatBarClick(TObject *Sender)
{
// Hide / show format bar (CoolBar->Bands->Items[1])
ViewFormatBar->Checked = !ViewFormatBar->Checked; //toggle
PopupFBar->Checked = !PopupFBar->Checked;
CoolBar->Bands->Items[1]->Visible = ViewFormatBar->Checked;
}
//--------------------------------------------------------------------------

void __fastcall TScratchPad::ViewStatusBarClick(TObject *Sender)
{
// Hide / show status bar (StatusBar)
ViewStatusBar->Checked = !ViewStatusBar->Checked; //toggle
PopupSBar->Checked = !PopupSBar->Checked;
StatusBar->Visible = ViewStatusBar->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::ViewOptionsClick(TObject *Sender)
{
OptionsBox->ShowModal();
}
//---------------------------------------------------------------------------

// Format Bar Control event handlers

// note that if the font is changed via the Font or Size
//  ComboBoxs, the changes are AUTOMATICALLY reflected
//  when the font dialog is displayed

void __fastcall TScratchPad::FontBoxClick(TObject *Sender)
{
// change the font
if(FontBox->Text != "")
 {
 // change typeface name
 Memo->SelAttributes->Name = FontBox->Text;

 // Finally, set the input focus to Memo component
 ScratchPad->FocusControl(Memo);
 }
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::SizeBoxKeyPress(TObject *Sender, char &Key)
{
// intercept chars for sizebox and filter out non-numeric keys
// if the user presses enter, change the font size
if(Key == '\r' && SizeBox->Text != "")
 {
 // use ToIntDef to return the original value for .size if string isn't an int
 Memo->SelAttributes->Size = SizeBox->Text.ToIntDef(Memo->Font->Size);

 // set the text value to reflect the actual size of the font
 SizeBox->Text = Memo->SelAttributes->Size;

 // we also want to select the text the user typed into the SizeBox
 SizeBox->SelStart = 0;
 SizeBox->SelLength = SizeBox->Text.Length(); // select all text

 // Finally, set the input focus to Memo component
 ScratchPad->FocusControl(Memo);
 }
else if((Key < '0' || Key > '9') && Key != '\b')
 Key = 0; // allow only 0-9 and backspace
return;
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::SizeBoxClick(TObject *Sender)
{
if(SizeBox->Text != "")
 {
 // use ToIntDef to return the original value for .size if string isn't an int
 Memo->SelAttributes->Size = SizeBox->Text.ToIntDef(Memo->Font->Size);

 // set the text value to reflect the actual size of the font
 SizeBox->Text = Memo->SelAttributes->Size;

 // Finally, set the input focus to Memo component
 ScratchPad->FocusControl(Memo);
 }
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FontBoldBtnClick(TObject *Sender)
{
// Toggle bold text
TFontStyles TFB = Memo->SelAttributes->Style;
if(!Memo->SelAttributes->Style.Contains(fsBold))
 TFB << fsBold; // add Bold to set
else
 TFB >> fsBold; // remove Bold from set
Memo->SelAttributes->Style = TFB;
// may have been invoked by shortcut - refresh button
MemoSelectionChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FontItalicBtnClick(TObject *Sender)
{
// Toggle italics
TFontStyles TFI = Memo->SelAttributes->Style;
if(!Memo->SelAttributes->Style.Contains(fsItalic))
 TFI << fsItalic; // add Italics to set
else
 TFI >> fsItalic; // remove Italics from set
Memo->SelAttributes->Style = TFI;
// may have been invoked by shortcut - refresh button
MemoSelectionChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FontUnderBtnClick(TObject *Sender)
{
// Toggle underlineing
TFontStyles TFU = Memo->SelAttributes->Style;
if(!Memo->SelAttributes->Style.Contains(fsUnderline))
 TFU << fsUnderline; // add underline to set
else
 TFU >> fsUnderline; // remove underline from set
Memo->SelAttributes->Style = TFU;
// may have been invoked by shortcut - refresh button
MemoSelectionChange(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TScratchPad::AlignLeftBtnClick(TObject *Sender)
{
// set alignment to left justify
Memo->Paragraph->Alignment = taLeftJustify;
// may have been invoked by shortcut - refresh button
MemoSelectionChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::AlignCenterBtnClick(TObject *Sender)
{
// set alignment to center
Memo->Paragraph->Alignment = taCenter;
// may have been invoked by shortcut - refresh button
MemoSelectionChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::AlignRightBtnClick(TObject *Sender)
{
// set alignment to right justify
Memo->Paragraph->Alignment = taRightJustify;
// may have been invoked by shortcut - refresh button
MemoSelectionChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::BulletBtnClick(TObject *Sender)
{
// update bullets menu item
FormatBullets->Checked = BulletBtn->Down;
if(BulletBtn->Down == true)
 Memo->Paragraph->Numbering = nsBullet;
else
 Memo->Paragraph->Numbering = nsNone;
// may have been invoked by shortcut - refresh button
MemoSelectionChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FontBoxDrawItem(TWinControl *Control,
      int Index, TRect &Rect, TOwnerDrawState State)
{
// draw an item for the font box
TCanvas& FBCanvas = *FontBox->Canvas;
// we're going to alter the rect a little, so create a temp
TRect temp = Rect;
FBCanvas.Brush->Style = bsClear;
if(State.Contains(odSelected) && FontBox->DroppedDown == true)
 {
 FBCanvas.Brush->Color = clHighlight;
 FBCanvas.FillRect(temp);
 // FBCanvas.DrawFocusRect(temp);
 FBCanvas.Font->Color = clHighlightText;
 // setup the preview font
 PopupEdit->Font->Name = FontBox->Items->Strings[Index];
 }
else
 {
 // I can't believe I didn't thkink of this before: when
 //  the FontBox is closed, we'll recieve a OnDrawItem event
 //  So all we have to do is hide the PopupEdit box
 if(!(FontBox->DroppedDown))
  PopupEdit->Visible = false; 
 FBCanvas.Font->Color = clBlack;
 FBCanvas.Brush->Color = clWhite;
 FBCanvas.FillRect(temp);
 }
// move everything over a bit so the FocusRect doesn't get in the way
// temp.Left += 2;
FBCanvas.Draw(temp.Left,temp.Top,TTFont);
temp.Left += 18;
DrawText(FBCanvas.Handle, FontBox->Items->Strings[Index].c_str(),
 -1, (RECT*)&temp, DT_VCENTER|DT_SINGLELINE);
}
//---------------------------------------------------------------------------

void __fastcall TScratchPad::FontBoxDropDown(TObject *Sender)
{
// set the position for the PopupEdit component
TRect PRect = FontBox->BoundsRect;
// to get left pos: pos. of format bar relative to window +
//  pos. of font box relative to format bar + 2
PRect.Left = FormatBar->Left + PRect.Right + 3;
// same deal for top, but move PopupEdit down into Memo
PRect.Top = FormatBar->Top + PRect.Bottom + 8;
PRect.Right = PRect.Left + 1; // arbitrary
PRect.Bottom = PRect.Top + 1;
// adjust the preview font - we can use Assign to copy the
//  TTextAttributes to TFont! Wow!
PopupEdit->Font->Assign(Memo->SelAttributes);
if(Memo->SelLength > 0)
 {
 // limit the length of the preview text to 20 chars
 PopupEdit->Caption = Memo->SelText.SubString(0,
  MAX_PREVIEW_LENGTH);
 }
else
 PopupEdit->Caption = PreviewText;
// For some reason, DrawText w/ CALCRECT doesn't work
//  correctly with PopupEdit->Handle; use Canvas->Handle
TFont* SaveFont = Canvas->Font;
Canvas->Font = PopupEdit->Font;
// Canvas->Font->Name = "Fixedsys";
DrawText(Canvas->Handle,PopupEdit->Caption.c_str(),-1,
 (RECT*)&PRect,DT_CALCRECT|DT_SINGLELINE);
Canvas->Font = SaveFont;
// make some adjustments to make PopupEdit easier to read
PRect.Right += 20;
PRect.Bottom += 4;
PopupEdit->BoundsRect = PRect;
PopupEdit->Visible = true;
}
//---------------------------------------------------------------------------

// Here's our method for opening files
// returns true for success
// this proc is incredibly slow!
bool TScratchPad::OpenFile(AnsiString OFile)
{
// make sure the file exists
if(FileExists(OFile))
 {
  // first, add the current file to the history list
 AddToHistory(SaveDialog->FileName);
 DrawHistory(); // redraw the new history list
 // clear the rich edit component
 if(Memo->Lines->Count > 0)
  Memo->Clear();
 // need to setup save dialog before RefreshFont!
 SaveDialog->FileName = OFile;
 // before we open the file, see if it is .rtf or nor
 if(ExtractFileExt(OFile).UpperCase() != ".RTF")
  {
  Memo->PlainText = true; // save as a plain text file
  RefreshFont(); // adjust Memo->Font
  }
 else // leave rtf files alone - font is already set
  {
  Memo->PlainText = false; // save as a rich text file
  Memo->WordWrap = true;   // just a temporary patch
  }
 // setup caption
 ScratchPad->Caption = ("RichPad - " + OFile);
 // load the file last - looks better!
 Memo->Lines->LoadFromFile(OFile);
 ClearModified(true); // clear modified flags
 return true;
 }
else
 {
 // tell the user that the file doesn't exist
 Application->MessageBox(
     "The file you selected does not exist.",
      "RichPad Message", MB_OK | MB_ICONWARNING);
 return false;
 }
}
//---------------------------------------------------------------------------

// File History Stuff
void TScratchPad::DrawHistory(void)
{
// draw the history list
// we have two sets of options in CurrOptions
// FullPath - show only the file's name or full path
// *InFileMenu - if false, draw in Reopen menu - IMPLEMENT*

// IN OUR FOR LOOPS, DO WE NEED < OR <= !!!???!!!

// We seem to get a lot of exceptions when trying to draw
//  the history list after an item has been removed!  We
//  are going to make sure all of the items in the list
//  at least exist
AnsiString temp;
for(int i = 0;i < MruCount;i++)
 {
 try
  {
  temp = MruList->Strings[i];
  }
 catch(...)
  {
  MruList->Add("");
  }
 }

int index = MRUTop->MenuIndex + 1; // index for first item
TMenuItem* MRUItem; // pointer for new MenuItems
// before we do anything else, clear the file menu
// Explanation: when we delete an item above MRUBottom,
//  MRUBottom's index decrements.  So we cannot inc index!
// Let MRUBottom's index move up toward us!
while(index < MRUBottom->MenuIndex)
 FileMenu->Delete(index);
MRUBottom->Visible = false; // hide for now
index = MRUTop->MenuIndex + 1; // restore index
for(int i=0;i<MruCount;i++)
 {
 if(MruList->Strings[i] != "")
  {
  MRUBottom->Visible = true; // show bottom seperator
  // create a new item with the file menu as the owner
  MRUItem = new TMenuItem(FileMenu);
  // draw the list based on the FullPath option
  if(CurrOptions->FullPath == true)
   {
   MRUItem->Caption = "&" + AnsiString(i) + " " +
    MruList->Strings[i];
   }
  else
   {
   MRUItem->Caption = "&" + AnsiString(i) + " " +
    ExtractFileName(MruList->Strings[i]);
   }
  // set the menu items event handler
  MRUItem->OnClick = MRUClick;
  FileMenu->Insert(index + i, MRUItem);
  }
 }
}
//---------------------------------------------------------------------------

void TScratchPad::AddToHistory(AnsiString FileToAdd)
{
if(FileToAdd != "") // don't try to add a null filename!
 {
 // see if the file is already in the list
 int slIndex = MruList->IndexOf(FileToAdd);
 if(slIndex != -1)
  MruList->Delete(slIndex); // delete the original entry
 // now add the file to the top of the list
 MruList->Insert(0, FileToAdd); // no duplicate entry
 if(MruList->Count > MruCount)
  MruList->Delete(MruCount);  // chop last item
 }
}

void TScratchPad::ClearHistory()
{
MruList->Clear();
// iterate through StringList, create each item and save
for (int i=0;i<MruCount;i++) // clear the StringList
 MruList->Add("");
RichPadKey->WriteString("MRUList", MruList->Text);
DrawHistory();
}
//---------------------------------------------------------------------------

void TScratchPad::DeleteKey()
{
RichPadKey->DeleteKey(RegKey);
RichPadKey->OpenKey(RegKey, true); // recreate empty key
}
//---------------------------------------------------------------------------

RichPadOptions* TScratchPad::GetOptions()
{
// read the current options - simply a getter
return CurrOptions;
}
//---------------------------------------------------------------------------

void TScratchPad::SetOptions(RichPadOptions* NewOptions)
{
// its simple enough to read options, but writing is a
//  different story - we have to adjust some stuff
int itemDiff = NewOptions->HistItems - CurrOptions->HistItems;
if(itemDiff > 0)
 {
 // adding items
 for(int i = 0;i < itemDiff;i++)
  MruList->Add("");
 }
else if(itemDiff < 0)
 {
 // removing items
 for(int i = 0;i < itemDiff;i++)
  MruList->Delete(MruList->Count - 1);
 }
MruCount = NewOptions->HistItems;
// now handle the font options
//SelFont->SaveFontStyle = NewOptions->SaveStyle;
//SelFont->SaveFontColor = NewOptions->SaveColor;
// simple copy the rest of the data
memcpy(CurrOptions, NewOptions, sizeof(RichPadOptions));
// draw the history list in case something changed
DrawHistory();
//RefreshFont(); // changes to Memo->WordWrap
}
//---------------------------------------------------------------------------

void TScratchPad::RefreshFont()
{
// adjust Memo->Font based on default font settings
// test the SaveDialog->Filename of the current file:
//  Unnamed - use DefNewFont
//  .txt - use DefTXTFont
//  All others - use DefOtherFont

if(SaveDialog->FileName != "")
 {
 if(ExtractFileExt(SaveDialog->FileName).UpperCase()
     == ".TXT") // .txt files
  {
  TXTFont->SetFont(Memo->Font);
  Memo->WordWrap = CurrOptions->TXTFile.WordWrap; // this causes real problems!?!
  ViewToolbar->Checked = CurrOptions->TXTFile.ShowToolbar; // toggle
  ViewFormatBar->Checked = CurrOptions->TXTFile.ShowFBar;
  ViewStatusBar->Checked = CurrOptions->TXTFile.ShowStatBar;
  }
 else // all other file types
  {
  OtherFont->SetFont(Memo->Font);
  Memo->WordWrap = CurrOptions->OtherFile.WordWrap;
  ViewToolbar->Checked = CurrOptions->OtherFile.ShowToolbar; // toggle
  ViewFormatBar->Checked = CurrOptions->OtherFile.ShowFBar;
  ViewStatusBar->Checked = CurrOptions->OtherFile.ShowStatBar;
  }
 }
else // new, unsaved files
 {
 NewFont->SetFont(Memo->Font);
 Memo->WordWrap = CurrOptions->NewFile.WordWrap;
 ViewToolbar->Checked = CurrOptions->NewFile.ShowToolbar; // toggle
 ViewFormatBar->Checked = CurrOptions->NewFile.ShowFBar;
 ViewStatusBar->Checked = CurrOptions->NewFile.ShowStatBar; 
 }
// setup everything else
// toolbar
PopupTBar->Checked = ViewToolbar->Checked;
CoolBar->Bands->Items[0]->Visible = ViewToolbar->Checked;
// format bar
PopupFBar->Checked = ViewFormatBar->Checked;
CoolBar->Bands->Items[1]->Visible = ViewFormatBar->Checked;
// status bar
PopupSBar->Checked = ViewStatusBar->Checked;
StatusBar->Visible = ViewStatusBar->Checked;
// because new files start out as rich text files, changing
//  Memo->Font has no effect because of the rtf font codes
//  - changing SelAttributes takes care of this.
Memo->SelAttributes->Assign(Memo->Font);
FormatWordWrap->Checked = Memo->WordWrap;
MemoSelectionChange(0); // refresh format bar
}
//---------------------------------------------------------------------------

void TScratchPad::ResetDefFont()
{
// reset default fonts by deleting registry values
RichPadKey->DeleteValue(DefNewFont);
RichPadKey->DeleteValue(DefTXTFont);
RichPadKey->DeleteValue(DefOtherFont);
}
//---------------------------------------------------------------------------


void __fastcall TScratchPad::FormatReformatClick(TObject *Sender)
{
ReformatBox->ShowModal();
}
//---------------------------------------------------------------------------

