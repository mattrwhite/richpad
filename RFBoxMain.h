//----------------------------------------------------------------------------
#ifndef RFBoxMainH
#define RFBoxMainH
//----------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <Messages.hpp>
#include <Classes.hpp>
#include <Graphics.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Forms.hpp>
//----------------------------------------------------------------------------
class TReformatBox : public TForm
{
__published:
    TButton *RFBoxOK;
    TButton *RFBoxCancel;
        TCheckBox *RemoveCRLF;
        TCheckBox *RemoveTabs;
    void __fastcall RFBoxCancelClick(TObject *Sender);
    void __fastcall RFBoxOKClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
private:
public:
	virtual __fastcall TReformatBox(TComponent *Owner);
};
//----------------------------------------------------------------------------
extern PACKAGE TReformatBox *ReformatBox;
//----------------------------------------------------------------------------
#endif
