//---------------------------------------------------------------------------
#include <vcl.h>
#include <string.h>
#pragma hdrstop

#include "RichFonts.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

// SaveFont - save the current font info to the value
//  specified by SValue to the open key indicated by SaveKey

bool TRichFonts::SaveFont(TRegistry* SaveKey,
 AnsiString SValue)
{
// to save the font to the registry, we create a structure
//  containing no pointers and save it as binary data
tagSaveFont* SaveFont = new tagSaveFont;
// copy all our data to the structure
SaveFont->sCharset = Charset;
SaveFont->sColor = (int)Color;
SaveFont->sSize = Size;
strcpy(SaveFont->sName, Name.c_str());

if(FontStyle.Contains(fsBold))
 SaveFont->sBold = true;
else
 SaveFont->sBold = false;
if(FontStyle.Contains(fsItalic))
 SaveFont->sItalic = true;
else
 SaveFont->sItalic = false;
if(FontStyle.Contains(fsUnderline))
 SaveFont->sUnderline = true;
else
 SaveFont->sUnderline = false;
if(FontStyle.Contains(fsStrikeOut))
 SaveFont->sStrikeOut = true;
else
 SaveFont->sStrikeOut = false;

SaveKey->WriteBinaryData(SValue, SaveFont,
 sizeof(tagSaveFont));
delete SaveFont;
return true;
}

// RestoreFont - set the properties of the TRichFonts from
//  the registry

bool TRichFonts::RestoreFont(TRegistry* RestoreKey,
 AnsiString RValue)
{
tagSaveFont* RestoreFont = new tagSaveFont;
// read the tagSaveFont data from the Registry
try
{
RestoreKey->ReadBinaryData(RValue, RestoreFont,
 sizeof(tagSaveFont));
}
catch(...)
{
return false; // failed
}
Charset = RestoreFont->sCharset;
Color = RestoreFont->sColor;
Name = AnsiString(RestoreFont->sName);
Size = RestoreFont->sSize;
FontStyle.Clear();
if(RestoreFont->sBold)
 FontStyle << fsBold;
if(RestoreFont->sItalic)
 FontStyle << fsItalic;
if(RestoreFont->sUnderline)
 FontStyle << fsUnderline;
if(RestoreFont->sStrikeOut)
 FontStyle << fsStrikeOut;
 
delete RestoreFont;
return true;
}

// StoreFont(TFont) - store the properties of a TFont object

bool TRichFonts::StoreFont(TFont* SFont)
{
Charset = SFont->Charset;
Color = SFont->Color;
Name = SFont->Name;
Size = SFont->Size;
// do the Styles property
FontStyle = SFont->Style;

return true;
}

// StoreFont(TTextAttributes) - store the properties of a
//  TTextAttrinutes object

bool TRichFonts::StoreFont(TTextAttributes* SAttrib)
{
Charset = SAttrib->Charset;
Color = SAttrib->Color;
Name = SAttrib->Name;
Size = SAttrib->Size;
FontStyle = SAttrib->Style;

return true;
}

// SetFont(TFont) - set the properties of the passed TFont
//  objects according to our stored info

bool TRichFonts::SetFont(TFont* SFont)
{
SFont->Charset = Charset;
SFont->Name = Name;
SFont->Size = Size;
if(SaveFontColor)
 SFont->Color = Color;
if(SaveFontStyle)
 SFont->Style = FontStyle;
return true;
}

// SetFont(TTextAttributes) - set the properties of the
//  passed TTextAttributes objects according to our stored
//  info

bool TRichFonts::SetFont(TTextAttributes* SAttrib)
{
SAttrib->Charset = Charset;
SAttrib->Name = Name;
SAttrib->Size = Size;
if(SaveFontColor)
 SAttrib->Color = Color;
if(SaveFontStyle)
 SAttrib->Style = FontStyle;
return true;
}


