//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USEFORM("RichMain.cpp", ScratchPad);
USEFORM("About.cpp", AboutBox);
USEFORM("Options.cpp", OptionsBox);
USEUNIT("RichFonts.cpp");
USEFORM("RFBoxMain.cpp", ReformatBox);
USERES("RichPad.res");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "RichPad";
                 Application->CreateForm(__classid(TScratchPad), &ScratchPad);
                 Application->CreateForm(__classid(TAboutBox), &AboutBox);
                 Application->CreateForm(__classid(TOptionsBox), &OptionsBox);
                 Application->CreateForm(__classid(TReformatBox), &ReformatBox);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
