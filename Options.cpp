//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Options.h"
#include "RichMain.h"
//---------------------------------------------------------------------
#pragma link "cspin"
#pragma resource "*.dfm"
TOptionsBox *OptionsBox;
RichPadOptions* CurrOptions;
RichPadOptions* TempOptions;
int LastIndex = 3; // for combo box
//--------------------------------------------------------------------- 
__fastcall TOptionsBox::TOptionsBox(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
// We get CurrOptions from ScratchPad->GetOptions(); and
//  copy it to a temporary struct, which we pass to RichMain
//  if the user clicks OK

void __fastcall TOptionsBox::FormShow(TObject *Sender)
{
// get the current options and display them
CurrOptions = ScratchPad->GetOptions();
// allocate the temporary RichPadOptions - we can't change
//  CurrOptions!
TempOptions = new ::RichPadOptions;
// Hey dumbass! Real smart passing RichPadOptions to sizeof -
//  that returns the size of the pointer, not the struct!
memcpy(TempOptions, CurrOptions, sizeof(*CurrOptions));
// set options for File History page
if(CurrOptions->FullPath)
 StyleGroup->ItemIndex = 0;
else
 StyleGroup->ItemIndex = 1;

if(CurrOptions->InFileMenu)
 LocationGroup->ItemIndex = 0;
else
 LocationGroup->ItemIndex = 1;

HistoryItems->Value = CurrOptions->HistItems;
// do Font Options
DefFontCBox->ItemIndex = 0;
DefFontCBoxClick(Sender);
SaveStyle->Checked = CurrOptions->SaveStyle; // ignore for now
SaveColor->Checked = CurrOptions->SaveColor;
}
//---------------------------------------------------------------------------
// OK click vs. Cancel button click - only affects items
//  contained in RichPadOptions!

void __fastcall TOptionsBox::OKBtnClick(TObject *Sender)
{
// read the options and call RichMain
if(StyleGroup->ItemIndex == 0)
 TempOptions->FullPath = true;
else
 TempOptions->FullPath = false;

if(LocationGroup->ItemIndex == 0)
 TempOptions->InFileMenu = true;
else
 TempOptions->InFileMenu = false;

TempOptions->HistItems = HistoryItems->Value;
// do Font options
TempOptions->SaveStyle = SaveStyle->Checked;
TempOptions->SaveColor = SaveColor->Checked;
// get last set of Wordwrap, etc. options
ReadCheckBoxes(LastIndex);
ScratchPad->SetOptions(TempOptions);
delete TempOptions; // always forget about this!
}
//---------------------------------------------------------------------------

void __fastcall TOptionsBox::ClearHistoryClick(TObject *Sender)
{
ScratchPad->ClearHistory(); // clear the history list
}
//---------------------------------------------------------------------------

void __fastcall TOptionsBox::CancelBtnClick(TObject *Sender)
{
OptionsBox->Close();
delete TempOptions;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsBox::DeleteKeyClick(TObject *Sender)
{
// warn the user
if(Application->MessageBox(
    "Are you sure you want to delete RichPad's key?",
    "RichPad Warning",
    MB_YESNO | MB_ICONWARNING | MB_DEFBUTTON2) == IDYES)
ScratchPad->DeleteKey();
}
//---------------------------------------------------------------------------

void __fastcall TOptionsBox::SetFontBtnClick(TObject *Sender)
{
// setup Font dialog according to selection in combo box
if(DefFontCBox->ItemIndex == 0) // new file
 ScratchPad->NewFont->SetFont(DefFontDialog->Font);
if(DefFontCBox->ItemIndex == 1) // .txt file
 ScratchPad->TXTFont->SetFont(DefFontDialog->Font);
if(DefFontCBox->ItemIndex == 2) // other files
 ScratchPad->OtherFont->SetFont(DefFontDialog->Font);
// Show the font dialog
if(DefFontDialog->Execute())
 {
 if(DefFontCBox->ItemIndex == 0) // new file
  ScratchPad->NewFont->StoreFont(DefFontDialog->Font);
 if(DefFontCBox->ItemIndex == 1) // .txt file
  ScratchPad->TXTFont->StoreFont(DefFontDialog->Font);
 if(DefFontCBox->ItemIndex == 2) // other files
  ScratchPad->OtherFont->StoreFont(DefFontDialog->Font);
 ScratchPad->RefreshFont(); // should we do this?
 }
}
//---------------------------------------------------------------------------

void __fastcall TOptionsBox::DefFontResetBtnClick(TObject *Sender)
{
// delete default font keys
if(Application->MessageBox(
    "Are you sure you want to reset the default fonts?",
    "RichPad Warning",
    MB_YESNO | MB_ICONWARNING | MB_DEFBUTTON2) == IDYES)
ScratchPad->ResetDefFont();
}
//---------------------------------------------------------------------------

void __fastcall TOptionsBox::DefFontCBoxClick(TObject *Sender)
{
// save current settings
ReadCheckBoxes(LastIndex);
// need to update Word wrap check box
// changing ->Checked activates OnClick for check box!
if(DefFontCBox->ItemIndex == 0) // new file
 {
 DefWordWrap->Checked = TempOptions->NewFile.WordWrap;
 DefToolBar->Checked = TempOptions->NewFile.ShowToolbar;
 DefFormatBar->Checked = TempOptions->NewFile.ShowFBar;
 DefStatBar->Checked = TempOptions->NewFile.ShowStatBar;
 }
if(DefFontCBox->ItemIndex == 1) // .txt file
 {
 DefWordWrap->Checked = TempOptions->TXTFile.WordWrap;
 DefToolBar->Checked = TempOptions->TXTFile.ShowToolbar;
 DefFormatBar->Checked = TempOptions->TXTFile.ShowFBar;
 DefStatBar->Checked = TempOptions->TXTFile.ShowStatBar;
 }
if(DefFontCBox->ItemIndex == 2) // other files
 {
 DefWordWrap->Checked = TempOptions->OtherFile.WordWrap;
 DefToolBar->Checked = TempOptions->OtherFile.ShowToolbar;
 DefFormatBar->Checked = TempOptions->OtherFile.ShowFBar;
 DefStatBar->Checked = TempOptions->OtherFile.ShowStatBar;
 }
LastIndex = DefFontCBox->ItemIndex;
}
//---------------------------------------------------------------------------

void TOptionsBox::ReadCheckBoxes(int ListIndex)
{
// get settings for outgoing item
if(ListIndex == 0) // new file
 {
 TempOptions->NewFile.WordWrap = DefWordWrap->Checked;
 TempOptions->NewFile.ShowToolbar = DefToolBar->Checked;
 TempOptions->NewFile.ShowFBar = DefFormatBar->Checked;
 TempOptions->NewFile.ShowStatBar = DefStatBar->Checked;
 }
if(ListIndex == 1) // .txt file
 {
 TempOptions->TXTFile.WordWrap = DefWordWrap->Checked;
 TempOptions->TXTFile.ShowToolbar = DefToolBar->Checked;
 TempOptions->TXTFile.ShowFBar = DefFormatBar->Checked;
 TempOptions->TXTFile.ShowStatBar = DefStatBar->Checked;
 }
if(ListIndex == 2)// other files
 {
 TempOptions->OtherFile.WordWrap = DefWordWrap->Checked;
 TempOptions->OtherFile.ShowToolbar = DefToolBar->Checked;
 TempOptions->OtherFile.ShowFBar = DefFormatBar->Checked;
 TempOptions->OtherFile.ShowStatBar = DefStatBar->Checked;
 }
}
//---------------------------------------------------------------------------
