�
 TOPTIONSBOX 0@  TPF0TOptionsBox
OptionsBoxLeft� Top� BorderStylebsDialogCaptionRichPad OptionsClientHeight� ClientWidth\
ParentFont	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width\Height� AlignalClient
BevelOuterbvNoneBorderWidthParentColor	TabOrder  TPageControlRichPadOptionsLeftTopWidthRHeight� 
ActivePageFileTypeOptionsTabOrder  	TTabSheetHistoryOptionsCaptionFile History TLabelLabel1LeftTopPWidthaHeightCaptionItems in History List*:  TLabelLabel2LeftTopdWidthHeightCaption;*Note: Changing number of items may reset the history list!  TRadioGroup
StyleGroupLeftTopWidthyHeight9CaptionHistory StyleItems.Strings	Full PathFilename Only TabOrder   TRadioGroupLocationGroupLeft� TopWidth� Height9CaptionHistory LocationItems.StringsIn File MenuSeperate submenu TabOrder  
TCSpinEditHistoryItemsLeftpTopKWidth)HeightTabStop	MaxValue
MinValueParentColorTabOrderValue  TButtonClearHistoryLeft� TopJWidthaHeightCaptionClear History NowTabOrderOnClickClearHistoryClick   	TTabSheetFileTypeOptionsCaption
File Types 	TGroupBoxDefaultFontGroupLeft TopWidthHeightiCaptionDefault PropertiesTabOrder  TLabelLabel3Left� TopWidthHeightCaptionShow:  	TComboBoxDefFontCBoxLeftTopWidth� Height
ItemHeightItems.StringsNew FileText file (.txt)All Other Files TabOrder OnClickDefFontCBoxClick  TButton
SetFontBtnLeftTop0WidthKHeightCaptionSet FontTabOrderOnClickSetFontBtnClick  TButtonDefFontResetBtnLeftXTop0WidthKHeightCaption	Reset AllTabOrderOnClickDefFontResetBtnClick  	TCheckBoxDefWordWrapLeftTopPWidthaHeightCaption	Word WrapTabOrder  	TCheckBox
DefToolBarLeft� TopWidthQHeightCaptionToolbarTabOrder  	TCheckBoxDefFormatBarLeft� Top(WidthQHeightCaption
Format BarTabOrder  	TCheckBox
DefStatBarLeft� Top8WidthQHeightCaption
Status BarTabOrder    	TTabSheetMiscPageCaptionMisc TButton	DeleteKeyLeftTopWidthiHeightCaptionDelete Registry KeyTabOrder OnClickDeleteKeyClick  	TCheckBox	SaveColorLeft� TopWidthaHeightCaptionSave Font ColorEnabledTabOrder  	TCheckBox	SaveStyleLeft� TopWidthiHeightCaptionSave Font StyleEnabledTabOrder    TButtonOKBtnLeft� Top� WidthKHeightCaptionOKDefault	ModalResultTabOrderOnClick
OKBtnClick  TButton	CancelBtnLeftTop� WidthKHeightCancel	CaptionCancelModalResultTabOrderOnClickCancelBtnClick   TFontDialogDefFontDialogFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style MinFontSize MaxFontSize LeftTop�    